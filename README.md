# wormbase2rdf

Scripts for converting information from WormBase (https://wormbase.org/) into RDF.

# Scripts

- `phenotype2rdf.py` converts phenotype annotations (e.g. `phenotype_association.WS???.wb` from ftp://ftp.wormbase.org/pub/wormbase/releases/current-production-release/ONTOLOGY/) into RDF
    - `.wb` files are actually (deprecated) [GAF 2.0](http://geneontology.org/docs/go-annotation-file-gaf-format-2.0/) files.


# Resources

- [phenotype ontology](e.g. `phenotype_ontology.WS???.obo` from ftp://ftp.wormbase.org/pub/wormbase/releases/current-production-release/ONTOLOGY/)
    - convert to OWL using [robot](http://robot.obolibrary.org/) ([PMID:31357927](https://www.ncbi.nlm.nih.gov/pubmed/31357927))
- evidence code ontology


# Workflow

- convert phenotype annotations using `phenotype2rdf.py`
- convert phenotype ontology using robot
    - `robot convert --input phenotype_ontology.WS275.obo --output phenotype_ontology.WS275.owl`
- integrate
    - `phenotype_association.WS275.ttl`
    - `phenotype_ontology.WS275.owl`
    - evidence code ontology
    - WBPaper descriptions
    - WBRNAi descriptions

# Todo

- name of the relation between an annotation and a WBRNAi
- automate data generation workflow using snakemake
- retrieve WBRNAi and WBPaper info using the REST API and convert to RDF
    - `curl -X GET --header 'Accept: application/json' 'http://rest.wormbase.org/rest/field/gene/WBGene00015927/phenotype'`
- FAIR: generate the VoID description of the RDF dataset
