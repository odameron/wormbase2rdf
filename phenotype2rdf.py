#! /usr/bin/env python3

import argparse
import csv


evidence = {}
evidence["HDA"] = "eco:0007005"
evidence["IBA"] = "eco:0000318"
evidence["IC"] = "eco:0000001"
evidence["IDA"] = "eco:0000314"
evidence["IEA"] = "eco:0000501"
evidence["IEP"] = "eco:0000008"
evidence["IGC"] = "eco:0000317"
evidence["IGI"] = "eco:0000316"
evidence["IMP"] = "eco:0000315"
evidence["IPI"] = "eco:0000353"
evidence["ISA"] = "eco:0000247"
evidence["ISM"] = "eco:0000202"
evidence["ISS"] = "eco:0000044"
evidence["NAS"] = "eco:0000303"
evidence["ND"] = "eco:0000035"
evidence["PCS"] = "eco:0006017"
evidence["RCA"] = "eco:0000245"
evidence["TAS"] = "eco:0000304"
evidence["EXP"] = "eco:0000006"
evidence["IKR"] = "eco:0000320"
evidence["IMR"] = "eco:0000320"
evidence["IRD"] = "eco:0000321"
evidence["HEP"] = "eco:0007007"
evidence["HMP"] = "eco:0007001"
evidence["ISO"] = "eco:0000266"

def parseWBphenotype(filepath, taxon, wbVersion):
	with open(filepath.replace('.wb', '.ttl'), 'w') as ttlfile:
		#ttlfile.write()
		ttlfile.write('@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n')
		ttlfile.write('@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n')
		ttlfile.write('@prefix owl: <http://www.w3.org/2002/07/owl#>.\n')
		ttlfile.write('@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n')
		ttlfile.write('@prefix skos: <http://www.w3.org/2004/02/skos/core#>.\n')
		ttlfile.write('@prefix dcterms: <http://purl.org/dc/terms/> .\n')
		ttlfile.write('\n')
		#ttlfile.write('@prefix apid: <https://apid.dep.usal.es/>.\n')
		#ttlfile.write('@prefix mi: <http://purl.obolibrary.org/obo/MI_>.\n')
		ttlfile.write('@prefix eco: <http://purl.obolibrary.org/obo/ECO_>.\n')
		ttlfile.write('@prefix pubmed: <https://www.ncbi.nlm.nih.gov/pubmed/>.\n')
		ttlfile.write('@prefix taxon: <http://purl.uniprot.org/taxonomy/>.\n')
		ttlfile.write('@prefix sio: <http://semanticscience.org/resource/>.\n')
		ttlfile.write('@prefix so: <http://purl.obolibrary.org/obo/>.\n')
		ttlfile.write('@prefix up: <http://purl.uniprot.org/core/>.\n')
		ttlfile.write('@prefix uniprotkb: <http://purl.uniprot.org/uniprot/>.\n')
		#ttlfile.write('@prefix wp: <http://vocabularies.wikipathways.org/wp#>.\n')
		#ttlfile.write('@prefix bp3: <http://www.biopax.org/release/biopax-level3.owl#>.\n')
		ttlfile.write('\n')
		ttlfile.write('@prefix wb: <http://wormbase.org/>.\n')
		ttlfile.write('@prefix WBAnnot: <http://wormbase.org/gene/annotation/>.\n')
		ttlfile.write('@prefix WBGene: <http://wormbase.org/gene/>.\n')
		#ttlfile.write('@prefix WBPhenotype: <http://wormbase.org/phenotype>.\n')
		ttlfile.write('@prefix WBPhenotype: <http://purl.obolibrary.org/obo/WBPhenotype_>.\n')
		ttlfile.write('@prefix WBRef: <http://wormbase.org/ref/>.\n')

		ttlfile.write('@prefix CeDRE: <http://wormbase.org/>.\n')
		#ttlfile.write('@prefix dip: <http://dip.doe-mbi.ucla.edu/>.\n')
		#ttlfile.write('@prefix intact: <https://www.ebi.ac.uk/intact/interaction/>.\n')

		with open(filepath) as csvfile:
			gafreader = csv.reader(csvfile, delimiter='\t')

			lineNumber = 0
			for row in gafreader:
				if len(row) == 0:
					continue
				if row[0].startswith("!"):
					continue

				ttlfile.write('\n')
				#print('\n' + str(row))
				# interaction identifiers contain a reference to the taxon for avoiding conflicts
				currentIdent = 'WBAnnot:annot-{}-{}'.format(wbVersion, lineNumber)
				#print(currentIdent)
				ttlfile.write(currentIdent + ' rdf:type wb:PhenotypeAnnotation .\n') 
				#ttlfile.write(currentIdent + ' dcterms:creator "http://wormbase.org/" .\n') 

				geneIdent = 'WBGene:' + row[1]
				ttlfile.write(currentIdent + ' wb:refersToGene ' + geneIdent + ' .\n') 
				ttlfile.write(currentIdent + ' sio:SIO_000628 ' + geneIdent + ' . # refers to\n') 
				ttlfile.write(geneIdent + ' rdf:type wb:Gene .\n') 
				ttlfile.write(geneIdent + ' rdf:type sio:SIO_010035 . # Gene\n') 
				ttlfile.write(geneIdent + ' rdf:type so:SO_0000704 . # Gene\n') 

				geneSymbol = row[2]
				if geneSymbol != "":
					ttlfile.write(geneIdent + ' rdfs:label "' + geneSymbol + '" .\n') 

				qualifier = row[3]
				if qualifier != "":
					ttlfile.write(currentIdent + ' wb:qualifier wb:' + qualifier + ' .\n') 

				phenotypeIdent = row[4]
				if qualifier == "NOT":
					ttlfile.write(currentIdent + ' wb:refersToNotPhenotype ' + phenotypeIdent + ' .\n') 
					ttlfile.write(currentIdent + ' sio:SIO_000628 ' + phenotypeIdent + ' . # refers to\n') 
					ttlfile.write(geneIdent + ' wb:hasNotPhenotype ' + phenotypeIdent + ' .\n') 
				else:
					ttlfile.write(currentIdent + ' wb:refersToPhenotype ' + phenotypeIdent + ' .\n') 
					ttlfile.write(currentIdent + ' sio:SIO_000628 ' + phenotypeIdent + ' . # refers to\n')
					ttlfile.write(geneIdent + ' wb:hasPhenotype ' + phenotypeIdent + ' .\n') 
					ttlfile.write(geneIdent + ' sio:SIO_001279 ' + phenotypeIdent + ' . # has phenotype\n') 

				if row[5] != "":
					refList = row[5].split("|")
					for currentRef in refList:
						currentRef = currentRef.replace("WB_REF", "WBRef").replace("WB:", "wb:")
						ttlfile.write(currentIdent + ' sio:SIO_000772 ' + currentRef + ' . # has (bibliographic) evidence\n')

				evidenceCode = row[6]
				if evidenceCode in evidence.keys():
					ttlfile.write(currentIdent + ' wb:hasEvidenceCode ' + evidence[evidenceCode] + ' .\n')
				else:
					ttlfile.write(currentIdent + ' wb:hasEvidenceCode [ rdfs:label "' + evidenceCode + '" ] .\n')
				
				if row[7] != "":
					supportingSourceList = row[7].split("|")
					for supportingSource in supportingSourceList:
						supportingSource = supportingSource.replace("WB:", "wb:")
						ttlfile.write(currentIdent + ' wb:supportingSource ' + supportingSource + ' . \n')
					
				primarySequenceName = row[9]
				if primarySequenceName != "":
					ttlfile.write(geneIdent + ' wb:primarySequenceName "' + primarySequenceName + '" .\n') 

				primarySequenceSynonym = row[10]
				if primarySequenceSynonym != "":
					ttlfile.write(geneIdent + ' skos:altLabel "' + primarySequenceSynonym + '" .\n') 

				taxon = row[12]
				ttlfile.write(geneIdent + ' wb:taxon ' + taxon + ' .\n') 

				annotationDate = row[13]
				if annotationDate != "":
					annotationDate = "{}-{}-{}".format(annotationDate[:4], annotationDate[4:6], annotationDate[6:])
					ttlfile.write(currentIdent + ' wb:date "' + annotationDate + '"^^xsd:date . \n')

				annotationAuthor = row[14]
				if annotationAuthor != "":
					# handle sloppy fild by wormbase
					if annotationAuthor == "WB":
						annotationAuthor = "http://wormbase.org/"

					if ":" not in annotationAuthor:
						ttlfile.write(currentIdent + ' dcterms:creator "' + annotationAuthor + '" .\n')
					elif annotationAuthor.startwith("http"):
						ttlfile.write(currentIdent + ' dcterms:creator <' + annotationAuthor + '> .\n')
					else:
						# assume CURIE
						ttlfile.write(currentIdent + ' dcterms:creator ' + annotationAuthor + ' .\n')

				lineNumber += 1


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Convert a GAF2.0 phenotype annotation file from WormBase (extension .wb) into RDF.')
	parser.add_argument("phenotypeFilePath", help="path to the phenotype annotation file")
	parser.add_argument("taxonID", help="identifier for the taxon (C. elegans = 6239)")
	args = parser.parse_args()

	#wbVersion = 'current'
	wbVersion = 'WS3275'
	if args.phenotypeFilePath.count('.') >= 2:
		wbVersion = args.phenotypeFilePath.split('.')[1]

	parseWBphenotype(args.phenotypeFilePath, args.taxonID, wbVersion)
